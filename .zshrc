export ZSH="/home/vivek/.oh-my-zsh"
ZSH_THEME="robbyrussell"
DISABLE_AUTO_UPDATE=true
plugins=(git zsh-syntax-highlighting) 
source $ZSH/oh-my-zsh.sh

# SOME OF MY ALIASES
alias config="/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
alias dlaud="youtube-dl -x --audio-format mp3"
alias dlvid='youtube-dl -f "best[height<=720]"'
alias wd="cd /data/vivek"
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

# ALIASES FOR GIT
alias gst="git status"
alias ga="git add"
alias gc="git commit"
alias gp="git push"

# ALIASES FOR CONFIG
alias cs="config status"
alias ca="config add"
alias cc="config commit"
alias cp="config push"

# PATH FOR NODEJS
export PATH="/usr/local/lib/nodejs/node-v16.8.0-linux-x64/bin:$PATH"
